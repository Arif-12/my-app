RSpec.describe "something" do
  context "in one context" do
    it "does one thing" do
      expect(true).to be_truthy
    end
  end

  context "in another context" do
    it "does another thing" do
      expect(true).to be_falsey
    end
  end
end
